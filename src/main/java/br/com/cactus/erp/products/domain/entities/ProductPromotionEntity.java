package br.com.cactus.erp.products.domain.entities;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name = "TB_PRODUTO_PROMOCAO")
public class ProductPromotionEntity {

    private Long id;

    private ProductEntity product;

    @Column(name = "data_inicio")
    private LocalDateTime dataInitial;

    @Column(name = "data_fim")
    private LocalDateTime dataFinished;

    @Column(name = "quantidade_em_promocao")
    private BigDecimal amountInPromotion;

    @Column(name = "quantidade_maxima_cliente")
    private BigDecimal maximumQuantityClient;
}
