package br.com.cactus.erp.products.domain.repository;

import br.com.cactus.erp.products.domain.entities.GroupProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupProductRepository extends JpaRepository<GroupProductEntity, Long> {
}
