package br.com.cactus.erp.products.domain.entities;

import jakarta.persistence.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "TB_PRODUTO")
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "marca_produto")
    private Long marcaProduto;

    @Column(name = "grupo_produto")
    private Long grupoProduto;

    @Column(name = "unidade_produto")
    private Long unidadeProduto;

    @Column(name = "gtin")
    private String gtin;

    @Column(name = "codigo_interno")
    private String codigoInterno;

    @Column(name = "nome")
    private String nome;

    @Column(name = "descricao")
    private String descricao;

    @Column(name = "")
    private String descricaoVenda;

    @Column(name = "descricao_venda")
    private BigDecimal valorCompra;

    @Column(name = "valor_venda")
    private BigDecimal valorVenda;

    @Column(name = "preco_venda_minima")
    private BigDecimal precoVendaMinima;

    @Column(name = "")
    private BigDecimal precoSugerido;

    @Column(name = "preco_sugerido")
    private BigDecimal custoMedioLiquido;

    @Column(name = "preco_lucro_zero")
    private BigDecimal precoLucroZero;

    @Column(name = "preco_lucro_minimo")
    private BigDecimal precoLucroMinimo;

    @Column(name = "preco_lucro_maximo")
    private BigDecimal precoLucroMaximo;

    @Column(name = "quantidade_estoque")
    private BigDecimal quantidadeEstoque;

    @Column(name = "estoque_minimo")
    private BigDecimal estoqueMinimo;

    @Column(name = "estoque_maximo")
    private BigDecimal estoqueMaximo;

    @Column(name = "excluido")
    private String excluido;

    @Column(name = "inativo")
    private String inativo;

    @Column(name = "data_cadastro")
    private LocalDateTime dataCadastro;
}
