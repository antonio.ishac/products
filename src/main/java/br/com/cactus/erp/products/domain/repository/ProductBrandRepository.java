package br.com.cactus.erp.products.domain.repository;

import br.com.cactus.erp.products.domain.entities.ProductBrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductBrandRepository extends JpaRepository<ProductBrandEntity, Long> {
}
