package br.com.cactus.erp.products.domain.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@Data
@Table(name = "TB_UNIDADE_PRODUTO")
@Entity
public class UnityProductEntity {

    @EqualsAndHashCode.Include
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "sigla")
    private String acronym;

    @Column(name = "descricao")
    private String description;
}
