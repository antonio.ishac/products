package br.com.cactus.erp.products.domain.repository;

import br.com.cactus.erp.products.domain.entities.UnityProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UnityProductRepository extends JpaRepository<UnityProductEntity, Long> {
}
